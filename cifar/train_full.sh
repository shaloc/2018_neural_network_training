#!/usr/bin/env sh
set -e

TOOLS=../../build/tools

echo "Start training..."
$TOOLS/caffe train \
    --solver=./cifar10_full_solver.prototxt $@
echo "Phase 1 Finished!"
# reduce learning rate by factor of 10
#$TOOLS/caffe train \
#    --solver=./cifar10_full_solver_lr1.prototxt \
#    --snapshot=./cifar10_full_iter_60000.solverstate $@

# reduce learning rate by factor of 10
#$TOOLS/caffe train \
#    --solver=./cifar10_full_solver_lr2.prototxt \
#    --snapshot=./cifar10_full_iter_65000.solverstate $@
