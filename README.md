# 2018神经网络培训
[![License](https://img.shields.io/badge/license-BSD-blue.svg)](LICENSE)
## 2018, 南京
大概了解了一些常用的神经网络工具和层，最简单的训练方法和调参方向
## Tengine
一个ARM OPEN AI LAB开发的神经网络工具，在专用硬件上调用网络会比其他的工具快一点
参考[Tengine on GitHub](https://github.com/OAID/Tengine)
## Caffe-HRT
OPEN AI LAB调优的Caffe
参考[Caffe-HRT on GitHub](https://github.com/OAID/Caffe-HRT)
## License
我创建的内容继承这个项目中绝大部分的BSD证书，同时要求增加：
- 在修改后通知我
- 在发布后通知我

