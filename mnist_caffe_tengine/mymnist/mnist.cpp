#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <sys/time.h>

#include <cstdio>
#include <cstdlib>
//#include <fileops.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "tengine_c_api.h"
#include "model_config.hpp"
#include "common.hpp"
#include "cpu_device.h"

#define PRINT_TOP_NUM 5
#define MODEL_NAME "mnist"
#define IMAGE_FILE "test.png"
#define LABEL_FILE "lables.txt"

#define IMG_H 28
#define IMG_W 28
#define MEAN 
#define SCALE 1.f
#define REPEAT 1

using namespace std;

void LoadLabelFile(std::vector<std::string> &result, const char *fname)
{
    std::ifstream labels(fname);

    std::string line;
    while (std::getline(labels, line))
        result.push_back(line);
}

static inline bool PairCompare(const std::pair<float, int> &lhs,
                               const std::pair<float, int> &rhs)
{
    return lhs.first > rhs.first;
}


static inline std::vector<int> Argmax(const std::vector<float> &v, int N)
{
    std::vector<std::pair<float, int> > pairs;
    for (size_t i = 0; i < v.size(); ++i)
        pairs.push_back(std::make_pair(v[i], i));
    std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

    std::vector<int> result;
    for (int i = 0; i < N; ++i)
        result.push_back(pairs[i].second);
    return result;
}

void get_input_data(const char *image_file, float *input_data, int img_h, int img_w, const float* mean, float scale)
{
    cv::Mat sample = cv::imread(image_file, -1);
    if (sample.empty())
    {
        std::cerr << "Failed to read image file " << image_file << ".\n";
        return;
    }
    cv::Mat img;
    if (sample.channels() == 4)
    {
        cv::cvtColor(sample, img, cv::COLOR_BGRA2BGR);
    }
    else if (sample.channels() == 1)
    {
        cv::cvtColor(sample, img, cv::COLOR_GRAY2BGR);
    }
    else
    {
        img=sample;
    }

    cv::resize(img, img, cv::Size(img_h, img_w));
    img.convertTo(img, CV_32FC3);
    float *img_data = (float *)img.data;
    int hw = img_h * img_w;
    for (int h = 0; h < img_h; h++)
    {
        for (int w = 0; w < img_w; w++)
        {
            for (int c = 0; c < 3; c++)
            {
                input_data[c * hw + h * img_w + w] = (*img_data - mean[c])*scale;
                img_data++;
            }
        }
    }
}

void PrintTopLabels(const char *label_file, float *data)
{
    // load labels
    std::vector<std::string> labels;
    LoadLabelFile(labels, label_file);
    cout << "Label file loaded" << endl;

    float *end = data + 10;
    std::vector<float> result(data, end);
    std::vector<int> top_N = Argmax(result, PRINT_TOP_NUM);
    cout << "get top_N:" << top_N.size() << endl;
    for (unsigned int i = 0; i < top_N.size(); i++)
    {
        int idx = top_N[i];
        cout << idx << " ~ " << top_N[i] << endl; 
        //std::cout /*<< std::fixed << std::setprecision(4)*/ 
	if(idx >= 0 && idx <= 9) 
	{
             cout << result[idx] << " - \"" << labels[idx] << "\"\n";
        }
    }
}

int main(int argc, char* argv[]){
    /*
    const char* model_name = "squeezenet";
    const char* proto_file = "models/sqz.prototxt";
    const char* model_file = "models/squeezenet_v1.1.caffemodel";
    const char* label_file = "models/synset_words.txt";
    const char* image_file = "tests/images/cat.jpg";
    */
    const char* model_name = "mnist";
    const char* proto_file = "/home/rk/my_module/test.prototxt";
    const char* model_file = "/home/rk/my_module/mnist.caffemodel";
    const char* label_file = "/home/rk/my_module/labels.txt";
    const char* image_file = "/home/rk/my_module/00271.png";
    int img_h = 28;
    int img_w = 28;
    float mean[] = {104.007, 116.669, 122.679};
    float scale = 1.f;
    struct timeval t0, t1;
    float avg_time = 0.f;

    //init
    init_tengine_library();
    load_model(model_name, "caffe", proto_file, model_file);
    
    //create graph
    graph_t graph = create_runtime_graph("graph", model_name, NULL);

    //get input
    int img_size = img_h * img_w * 3;
    float*  input_data = (float *)malloc(sizeof(float)* img_size);
    tensor_t input_tensor = get_graph_input_tensor(graph, 0, 0);
    
    //prerun
    prerun_graph(graph);
    cout << "Prerun OK" << endl;
 
    get_input_data(image_file, input_data, img_h, img_w, mean, scale);
    set_tensor_buffer(input_tensor, input_data, img_size*4);

    gettimeofday(&t0, NULL);
    run_graph(graph, 1);
    gettimeofday(&t1, NULL);
    float mytime = (float)((t1.tv_sec * 1000000 + t1.tv_usec) - (t0.tv_sec * 1000000 + t0.tv_usec)) / 1000;
    avg_time += mytime;
    cout << "Runned" << endl;

    //print message
    std::cout << "\nModel name : " << model_name << "\n"
              << "Proto file : " << proto_file << "\n"
              << "Model file : " << model_file << "\n"
              << "label file : " << label_file << "\n"
              << "image file : " << image_file << "\n"
              << "img_h, imag_w, scale, mean[3] : " << img_h << " " << img_w << " "
              << scale << " " << mean[0] << " " << mean[1] << " " << mean[2] << "\n";
    std::cout << "--------------------------------------\n";

    //print output
    tensor_t output_tensor = get_graph_output_tensor(graph, 0, 0);
    float *data = (float *)get_tensor_buffer(output_tensor);
    cout << "Before print labels" << endl;
    PrintTopLabels(label_file, data);

    std::cout << "--------------------------------------\n";

    //free
    free(input_data);
    postrun_graph(graph);
    destroy_runtime_graph(graph);
    remove_model(model_name);

    return 0;

}
